# -*- coding: utf-8 -*-
########################################################################################################################
# This script splits up the dataset given to it into N parts and then runs N separate processes, each one running
# over a subset of the data. This speeds up analysis time substantially.
#
# Note: Please check the utilisation of the machine you intend to run on and scale the number of processes you
# wish to run based on this. Always aim to use less than half the number of cores the machine you are using has.
#
# If on CORI please check the information given regarding CPU intensive jobs on the login nodes.
#
# Example Useage 
# 
# python RunNJobs_localDataset.py -m ODPlots -n 10 -d /global/projecta/projectdirs/lz/data/MDC3/background/LZAP-4.4.0/20180401/
# 
#  This will run the ODPlots analysis in 10 seperate processes over the data given in the directory given then join the 
#  10 output files into one saved to the normal directory  (run/ODPlots/ODPlots_results.root)
#
#######################################################################################################################     
#   For help type,
#          python RunNAlpacaJobs.py -h    
#
#   Contact w.turner@liv.ac.uk 
#
########################################################################################################################
# TODO   
# check all things ALPACA needs to run has been given correctly by the user 

import os
import sys
import subprocess
import time
import random
import math
import argparse

from itertools import izip_longest

def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return list(izip_longest(*args, fillvalue=fillvalue))

def convertTuple(tup): 
    str =  ''.join(tup) 
    return str

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '▊', printEnd = ""):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = iteration
    filledLength = int(length * int(iteration) // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    barOutput = ' {}  |{}| {}% {} {}'.format(prefix, bar, percent, suffix, printEnd)
    return(barOutput)

# list the directory of the given dataset to get list of files to run over
def getDataSetList(dataPath):
    commands = '''ls {}'''.format(dataPath)
    try:
        out = subprocess.check_output(commands, shell=True)
        return out
    except Exception, e:
        print("Error listing input dataset")
        exit()

# Check to see if the analysis module the user gave is valid    
def checkModuleName(moduleName):
    moduleDir = ALPACADIR + "/modules"
    command = '''ls {}'''.format(moduleDir)
    
    try:
        out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    except Exception, e:
        print("Theres been an error listing the analysis modules... ")
        print(e)
        exit() #if there is an error, stop
    if moduleName in out.split():
        return
    else:
        print("Analyis Module given not found in list of valid analysis modules in this ALPACA directory...")
        exit()

def makeTmpDir():
    #try and make the tmp directory. if it exists remove the contents.
    command = '''mkdir {}/analysisScripts/tmp'''.format(ALPACADIR)
    try:
        out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    except Exception, e:
        print("Directory exists already. Empyting directory")
        command = '''rm {}/analysisScripts/tmp/*'''.format(ALPACADIR)
        try:
            out = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
        except Exception, e:
            print("Error cleaning previous tmp files")


# All functions declared.

# See if setup script has been run
try:
    ALPACADIR = os.environ['ALPACA_TOPDIR']
except Exception, e:
    print("Can not find ALPACA directory enviromental varaible. Please ensure setup script has been run.")
    exit()

scriptPath=ALPACADIR+"/analysisScripts/"

#
# Get command line arguments
#
ap = argparse.ArgumentParser(description='Run your analyses over the given dataset using N threads to process the data (each thread running over a set of files from the dataset, number of files per thread = (total number of files / num threads) and join the output. This script is to be run over data on CORI or local data on your own insistutions computing resources. There is a different script if you wish to run over a dataset streamed using XROOTD from  a data centre.')

# Add the arguments to the parser
ap.add_argument("-n", "--numJobsToRun", required=True, help="Number of sub processes to run. Each sub processes will run over (the number of files in the dataset / number of sub processes)", type=int)

ap.add_argument("-m", "--module", required=True,
                help="Name of the analysis module you want to run over the given dataset, NOTE: this must locally built.", type=str)

ap.add_argument("-o", "--outputName", required=False,
                help="Name of the joined output file - default is just the module name.", type=str)

ap.add_argument("-d", "--dataset", required=True,
                help="The path to the dataset. For example /lz/data/MDC3/calibration/LZAP-4.4.0/20180222/   If you want to use a wildcard to a subset of the dataset you can do this with surrounding the directory with ' '. For example '/lz/data/MDC3/calibration/LZAP-4.4.0/20180222/lz_12*'   NOTE: not back tick marks ` ` ", type=str)

ap.add_argument('-v', action='store_true', help="Leaves the temporary output log and root files in the tmp directory to look at incase of any errors.")

args = vars(ap.parse_args())

#get time as script starts
start_time = time.time()

# Get number of jobs to run and dataset path
numJobsToRun = args['numJobsToRun']
joinedRootFilename = args['outputName']
dataPath = args['dataset']

# see if wildcards are being used, wildcards will give full filepath rather than just filename
usingWildcard = False
if dataPath[-1:] == "*":
    print("Using Wildcard file selection")
    usingWildcard = True

if dataPath[-1:] != "/":
    if dataPath[-1:] != "*":
        dataPath = dataPath + "/"

print("Running {}".format(int(args['numJobsToRun'])) + str(" jobs over dataset {}".format(dataPath)))

#convert the list of files into a more useful list to use.
datasetList = convertTuple( getDataSetList(dataPath) ).split()

# Check module given 
checkModuleName(args['module'])

# make tmp dir to output log files and root files to
makeTmpDir()

#  !!!! shuffle the list of files, some data generated locally in Liverpool has uneven filesizes so number of events vary in jobs meaning some jobs take longer than others
# this fixes that however when running over normal dataset this shouldnt be needed and should be removed
random.shuffle(datasetList)
# !!!!!

# print some info about the jobs / datasets
print("Running analyses module, {}".format(args['module']))
print("Number of files in the input dataset, {}".format(len(datasetList)))
print("Number of Jobs to run : {}".format(numJobsToRun))
print("Number of Files per job : {}".format(len(datasetList)/numJobsToRun))

if numJobsToRun > len(datasetList):
    print("More jobs set to run than files in dataset. Reduce number of jobs.")
    exit()

# Stop users from running more than 48 threads
# Hello, if you have found this variable and wish to change it please make sure you know what you are doing
# you could very easily slow down analysis machines by changing this variable.
numJobsLimit = 48
# Again, don't change this variable unless you are happy to face the consequences

if (numJobsToRun > numJobsLimit):
    print("Too many jobs set to run")
    exit()

# split dataset to seperate lists for each job
splitUpDataset = grouper( int(math.ceil(float(len(datasetList))/float(numJobsToRun))), datasetList)


#make required dicts to store info
processNames = {}
logFileNames = {}
outputRootFileNames = {}

# for each dataset make a input file list, log file and start the job
for index, filenames in enumerate(splitUpDataset):
    processNum = "job_" + str(index)
    jobListFileName = scriptPath + "/tmp/job_" + str(index) + "_dataset.txt"
    logFileName = scriptPath + "/tmp/log_" + str(index) + ".txt"
    logFileNames[index] = logFileName
    outputRootFileName = processNum + "_output.root"

    jobDatasetListFile = open(jobListFileName,"w+")
    logfile = open(logFileName,"a")
    logfile.write("Starting \n")

    # for file in split dataset list, add file to the input file list
    for filename in enumerate(filenames):
        if filename[1] != None:
            nameOfFile = filename[1]
            if usingWildcard:
                fullPath = nameOfFile
            else:
                fullPath = dataPath + nameOfFile


            jobDatasetListFile.write(fullPath)
            jobDatasetListFile.write("\n")

    # this command is the ALPACA analysis that will be run
    #    args['module'] is the name of the ALPACA analysis
    # -i 500  means the progress of each job is printed out every 500 events so % of each job progress is printed out
    # -f means the input file list for each job is given. 
    # -o is the output directory for each job where the root files are found to be joined at the end
    cmd = str(args['module']  + " -i 500 -f " + str(jobListFileName) + " -o " + str(ALPACADIR) + '/analysisScripts/tmp/' + str(outputRootFileName))
    processNames[processNum] = subprocess.Popen(cmd, stdout=logfile, stderr=logfile, shell=True)
    outputRootFileNames[processNum] = outputRootFileName

    logfile.close()
    jobDatasetListFile.close()

# make required dicts
repeatNeeded = True
jobStatus = {}
lastLineFromJob = {}
totalNumEvents = {}

# get ready to print out job progress
print("Running {} parallel processes.... ".format(len(processNames))) 
print("")
print("")

print("Progress of Jobs...")

# print whitespace to output progress
for i in range(numJobsToRun+1):
    print()


timerStarted = False
startTime = 0
# this block gets the progress from each log file and prints this out
while repeatNeeded == True:
    for index, i in enumerate(processNames):
        poll = processNames[i].poll()
        jobStatus[i] = "Running"

        if poll is None:
            jobStatus[i] = "Running"

            logfile = open(logFileNames[index], "r")
            lineList = logfile.readlines()
            lastLine = lineList[-1]

            if "%" in lastLine:
                lastLineFromJob[i] = lastLine
            elif "Real" in lastLine: # log file shows completion of job
                lastLineFromJob[i] = ">>> (100%) <<<"
                
            logfile.close()
        else:
            jobStatus[i] = "Finished"
            lastLineFromJob[i] = ">>> (100%) <<<"

    numJobsFinished = sum(value == "Finished" for value in jobStatus.values())

    # clear previous lines
    for i in range(len(lastLineFromJob) + 1):
        sys.stdout.write("\033[F\x1b[K")        

    for key, value in lastLineFromJob.items():
        if "%" in value: 

            # Get Number of Events from process output, know its between two characters
            start = '>>>'
            end = '('
            line = value.rstrip().strip()
            numEvents = line[line.find(start)+len(start):line.rfind(end)]
            percentComplete = line[-9:-6] # get percent complete, doesnt change pos
            
            if numEvents > 0 and timerStarted == False:
                timerStarted = True
                startTime = time.time()

            # make progress bar 
            progress = convertTuple( printProgressBar(percentComplete, 100, prefix = 'Progress:', suffix = 'Complete', length = 50))

            # if not complete print event numbers
            if int(percentComplete) != 100:
                print("{:8s} {}- Event {} ".format(key, progress, numEvents))
                totalNumEvents[key] = int(numEvents)
            else:
                print(" {:8s} {} ".format(key, progress))
    
    numEvents = sum(totalNumEvents.values())
    if (numEvents != 0):
        print("Avg Rate = {} evt/s".format(int(round(numEvents/(time.time() - startTime)))))
    else:
        print("Avg Rate = {} evts/s".format(0))

    # when finished stop looping
    if numJobsToRun == numJobsFinished:
        repeatNeeded = False

    time.sleep(0.1)

print("")
print("All Jobs Finished")
print("")



# test to see if the output directory has been made  run/ModuleName/   if not hadd will not be able to save output file there. 
checkDirCommand = "mkdir " + ALPACADIR + "/run/" + args['module']
try:
    out = subprocess.check_output(checkDirCommand, stderr=subprocess.STDOUT, shell=True)
except Exception, e:
    print("Output run/{} directory exists already.".format(args['module']))

# Join root files together into one output file 
print("Joining Output Root Files ...")
print("")
if joinedRootFilename is None:
    joinedRootFilename = args['module'] + "Analysis.root"
    print("hadd -f {} {} /analysisScripts/tmp/*.root".format(joinedRootFilename, ALPACADIR))
    joinCommand = "hadd -f " + ALPACADIR + "/run/" + args['module'] + "/" + joinedRootFilename + " " + ALPACADIR + "/analysisScripts/tmp/*.root"
else:
    if ".root" not in joinedRootFilename:
        joinedRootFilename = joinedRootFilename + ".root"

    print("hadd -f {} {}/analysisScripts/tmp/*.root".format(joinedRootFilename, ALPACADIR))
    joinCommand = "hadd -f " + ALPACADIR + "/run/" + args['module'] + "/" + joinedRootFilename + " " + ALPACADIR + "/analysisScripts/tmp/*.root"
try:
    out = subprocess.check_output(joinCommand, stderr=subprocess.STDOUT, shell=True)
    print(out)
except Exception, e:
    print("Theres been an error during hadd... ")
    print("")
    print(e)

# if -v argument is given to python script command the output temp files will not be removed to allow users to check for errors
if args['v'] is False:
    print("Removing temporary files ")
    rmdirCommand = str("rm -r " + ALPACADIR + "/analysisScripts/tmp")
    try:
        output = subprocess.check_output(rmdirCommand, shell=True, stderr=subprocess.STDOUT)
    except Exception, e:
        print(e)
        print("Error removing previous files in {}/analysisScripts/tmp".format(ALPACADIR))
    
else:
    print("Not removing temporary files ")

print("Analysis Finished, took {} seconds ".format( round(time.time() - start_time, 2)))


