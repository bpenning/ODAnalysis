#ifndef CutsODAnalysis_H
#define CutsODAnalysis_H

#include "EventBase.h"

class CutsODAnalysis  {

public:
  CutsODAnalysis(EventBase* eventBase);
  ~CutsODAnalysis();
  bool ODAnalysisCutsOK();


private:
  
  EventBase* m_event;
    
};

#endif
