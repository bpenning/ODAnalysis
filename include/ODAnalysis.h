#ifndef ODAnalysis_H
#define ODAnalysis_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsODAnalysis.h"

#include <TTreeReader.h>
#include <TString.h>
#include <string>

class ODAnalysis: public Analysis {

public:
  ODAnalysis(); 
  ~ODAnalysis();

  void Initialize();
  void Execute();
  void Finalize();
  void FillPlots(string text);
  //  void FillPlots(TTreeReaderValue branch);

protected:
  int nEvents;
  CutsODAnalysis* m_cutsODAnalysis;
  ConfigSvc* m_conf;
    
public: 
    float od_subleading_leading_ratio;
    int od_max_coinc;
    int od_max_coinc_id;
    float od_max_area;
    int od_max_area_id;
    float od_subleading_area;
    int od_subleading_area_id;

    
    int tpc_s1_id;
    float tcp_s1_time;
};

#endif
