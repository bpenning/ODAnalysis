#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"

#include "CutsBase.h"
#include "CutsODAnalysis.h"
#include "ConfigSvc.h"
#include "ODAnalysis.h"

// Constructor
ODAnalysis::ODAnalysis()
  : Analysis()
{
  // List the branches required below, see full list here *:::: LINK TO SOFTWARE DOCS  ::::*
  // NOTE : if you try and use a variable in a branch that has NOT been loaded you will get a seg fault
  // there is not a error message for this currently. If you get a seg fault first thing to check 
  // is that you are including the required branch that variable lives on. 
  //
  m_event->IncludeBranch("pulsesODHG");
  m_event->IncludeBranch("pulsesTPCHG");
  m_event->IncludeBranch("ss");
  //
  ////////

  m_event->Initialize();

  // Setup loggingx
  logging::set_program_name("ODAnalysis Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  m_cutsODAnalysis = new CutsODAnalysis(m_event);
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();
}

// Destructor
ODAnalysis::~ODAnalysis() {
  delete m_cutsODAnalysis;
}

void ODAnalysis::FillPlots(string text){
  float radius=(float) sqrt(pow((*m_event->m_singleScatter)->x_cm,2)+pow((*m_event->m_singleScatter)->y_cm,2));
  float z_cm= ((*m_event->m_singleScatter)->driftTime_ns)*0.00015; //in cm

  m_hists->BookFillHist(text+"cutflow", 5, -1., 4, 1);    
      
  if(!(*m_event->m_singleScatter)->nSingleScatters) return;
  
  m_hists->BookFillHist(text+"tpc_HGpulses_n", 30, 0., 200., (*m_event->m_tpcHGPulses)->nPulses);
  m_hists->BookFillHist(text+"od_HGpulses_n", 120, 0., 600., (*m_event->m_odHGPulses)->nPulses);    

    
  m_hists->BookFillHist(text+"tpc_ss_s1", 30, 0., 1000, (*m_event->m_singleScatter)->s1Area_phd);
  m_hists->BookFillHist(text+"tpc_ss_s2", 30, 0., 1000, (*m_event->m_singleScatter)->s2Area_phd);
  m_hists->BookFillHist(text+"tpc_ss_x_vs_y", 200, -100, 100, 200, -100, 100, 
                        (*m_event->m_singleScatter)->x_cm, (*m_event->m_singleScatter)->y_cm);
  m_hists->BookFillHist(text+"tpc_ss_z", 30, 0., 120, z_cm);
  m_hists->BookFillHist(text+"tpc_ss_r", 30, 0., 100, radius);
  m_hists->BookFillHist(text+"tpc_ss_x_vs_z", 30, -100, 100, 30, 0., 120, (*m_event->m_singleScatter)->x_cm, z_cm);
  m_hists->BookFillHist(text+"tpc_ss_y_vs_z", 30, -100, 100, 30, 0., 120, (*m_event->m_singleScatter)->y_cm, z_cm);
  m_hists->BookFillHist(text+"tpc_ss_r_vs_z", 30, -100, 100, 30, -20., 120, radius, z_cm);
  m_hists->BookFillHist(text+"tpc_ss_area_s1_vs_logs2s1", 100, 0, 50, 100, 1, 10, (*m_event->m_singleScatter)->s1Area_phd,
			log((*m_event->m_singleScatter)->s2Area_phd/(*m_event->m_singleScatter)->s1Area_phd));


  
  //now process OD pulses
  // int maxCoin=0;
  // int maxCoinID=-1;  
  // float max_area=0;
  // int max_area_id=-1;
  // float subleading_area=0;
  // int subleading_area_id=-1;

    
  for (int i = 0; i < (*m_event->m_odHGPulses)->nPulses; i++) {
    
    if ((*m_event->m_odHGPulses)->pulseArea_phd[i]<3) continue;
      
    // //some derivative variables
    double width=(*m_event->m_odHGPulses)->pulseEndTime_ns[i] - (*m_event->m_odHGPulses)->pulseStartTime_ns[i];
    double h2w=(*m_event->m_odHGPulses)->peakAmp[i]/width;
    // if((*m_event->m_odHGPulses)->coincidence[i]>maxCoin){
    //   maxCoin=(*m_event->m_odHGPulses)->coincidence[i];
    //   maxCoinID=i;
    // }
    // // std::cout<<"i "<<i<<" ->coinc "<<(*m_event->m_odHGPulses)->coincidence[i]<<" maxCoin "<<maxCoin<<" od_max_coinc "<<od_max_coinc<<" maxCoinID "<<maxCoinID<<" od_max_coinc_id "<<od_max_coinc_id<<std::endl;
      
    // if((*m_event->m_odHGPulses)->pulseArea_phd[i]>max_area){
    //   subleading_area=max_area;
    //   subleading_area_id=max_area_id;
    //   max_area=(*m_event->m_odHGPulses)->pulseArea_phd[i];
    //   max_area_id=i;
    // } 
    
      
      
    //  if((*m_event->m_odHGPulses)->pulseArea_phd[i]>1 && text=="cut0_presel_") 
    //    std::cout<<nEvents<<" pulse area "<<i<<": "<<(*m_event->m_odHGPulses)->pulseArea_phd[i]
    //       <<" ("<<(*m_event->m_odHGPulses)->coincidence[i]<<")"<<std::endl;

      
    m_hists->BookFillHist(text+"od_pulseArea", 25, -10., 800, (*m_event->m_odHGPulses)->pulseArea_phd[i]);
    m_hists->BookFillHist(text+"od_pulseStartTime_ns", 20, -2E3, 2E3, (*m_event->m_odHGPulses)->pulseStartTime_ns[i]);
    m_hists->BookFillHist(text+"od_coincidence", 120, 0., 120, (*m_event->m_odHGPulses)->coincidence[i]);
    m_hists->BookFillHist(text+"od_peakAmp", 30, 0., 2, (*m_event->m_odHGPulses)->peakAmp[i]);
    m_hists->BookFillHist(text+"od_width", 60, 0., 1E3, 
			  (*m_event->m_odHGPulses)->pulseEndTime_ns[i]-(*m_event->m_odHGPulses)->pulseStartTime_ns[i]);
    m_hists->BookFillHist(text+"od_h2w", 60, 0., 0.04, h2w);
    m_hists->BookFillHist(text+"od_area_vs_h2w", 25, -10., 200, 120, 0., 0.04, (*m_event->m_odHGPulses)->pulseArea_phd[i], h2w);
    m_hists->BookFillHist(text+"od_area_vs_coinc", 25, -10., 200, 120, 0., 120, (*m_event->m_odHGPulses)->pulseArea_phd[i], (*m_event->m_odHGPulses)->coincidence[i]);
    m_hists->BookFillHist(text+"od_area_vs_width", 25, -10., 200, 120, 0., 1E3, (*m_event->m_odHGPulses)->pulseArea_phd[i], width);
    m_hists->BookFillHist(text+"od_peakAmp_vs_width", 30, 0., 2, 120, 0., 1E3, (*m_event->m_odHGPulses)->peakAmp[i], width);
    //if (abs(diff)<=100)
    m_hists->BookFillHist(text+"od_prompt_pulseArea", 25, -100., 800, (*m_event->m_odHGPulses)->pulseArea_phd[i]);

  }// loop over TPC pulses


  int tpc_s1_id=(*m_event->m_singleScatter)->s1PulseID;
  float tcp_s1_time=(*m_event->m_tpcHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID];
  float od_time_coinc=(*m_event->m_odHGPulses)->pulseStartTime_ns[od_max_coinc_id];
  float od_time_area=(*m_event->m_odHGPulses)->pulseStartTime_ns[od_max_area_id];

  double diff_area=abs(od_time_area-tcp_s1_time);
  double diff_coinc=abs(od_time_coinc-tcp_s1_time);

  m_hists->BookFillHist(text+"od_max_coin", 30, 0., 120, od_max_coinc);
  m_hists->BookFillHist(text+"od_max_coin_id", 30, 0., 120, od_max_coinc_id);
  m_hists->BookFillHist(text+"od_max_area", 30, 0., 20000, od_max_area);
  m_hists->BookFillHist(text+"od_max_area_id", 30, 0., 20000, od_max_area_id);
  m_hists->BookFillHist(text+"od_subleading_area", 30, 0., 1000, od_subleading_area);
  m_hists->BookFillHist(text+"od_subleading_area_id", 30, 0., 1000, od_subleading_area_id);
  
  m_hists->BookFillHist(text+"od_maxAreaID_vs_maxCoinID", 30, 0., 300, 30, 0., 300, od_max_area_id, od_max_coinc_id);
  m_hists->BookFillHist(text+"od_s1_diff_area", 30, 0., 1000, diff_area);
  m_hists->BookFillHist(text+"od_s1_diff_1sec_area", 40, 0., 1000000, diff_area);
  m_hists->BookFillHist(text+"od_s1_diff_coinc", 30, 0., 1000, diff_coinc);
  m_hists->BookFillHist(text+"od_s1_diff_1sec_coinc", 40, 0., 1000000, diff_coinc);

    
  //some leading/subleading ratio plots
  m_hists->BookFillHist(text+"od_leading_pulse_area", 25, -10., 800, (*m_event->m_odHGPulses)->pulseArea_phd[od_max_area_id]);
  m_hists->BookFillHist(text+"od_subleading_pulse_area", 25, -100., 400, (*m_event->m_odHGPulses)->pulseArea_phd[od_subleading_area_id]);
  if (od_subleading_leading_ratio>0)
    m_hists->BookFillHist(text+"od_subleading_leading_area_lsratio", 25, 0., 1, od_subleading_leading_ratio);
    
  //some max plots
  m_hists->BookFillHist(text+"od_leading_peakAmp_vs_area", 30, 0., 2, 50, -100., 800, (*m_event->m_odHGPulses)->peakAmp[od_max_area_id], (*m_event->m_odHGPulses)->pulseArea_phd[od_max_area_id]);
  m_hists->BookFillHist(text+"od_leading_area_vs_lsratio", 25, -100., 800, 50, 0., 1, (*m_event->m_odHGPulses)->pulseArea_phd[od_max_area_id], od_subleading_leading_ratio);
  m_hists->BookFillHist(text+"od_leading_area_vs_coinc", 25, -100., 800, 120, 0., 120, (*m_event->m_odHGPulses)->pulseArea_phd[od_max_area_id], (*m_event->m_odHGPulses)->coincidence[od_max_area_id]);

    
  //some subleading plots
  m_hists->BookFillHist(text+"od_subleading_peakAmp_vs_area", 30, 0., 2, 50, -100, 300, (*m_event->m_odHGPulses)->peakAmp[od_subleading_area_id], (*m_event->m_odHGPulses)->pulseArea_phd[od_subleading_area_id]);
  m_hists->BookFillHist(text+"od_subleading_area_vs_lsratio", 25, 0., 200, 50, 0., 1, (*m_event->m_odHGPulses)->pulseArea_phd[od_subleading_area_id], od_subleading_leading_ratio);
  m_hists->BookFillHist(text+"od_subleading_area_vs_coinc", 25, -100., 800, 120, 0., 120, (*m_event->m_odHGPulses)->pulseArea_phd[od_subleading_area_id], (*m_event->m_odHGPulses)->coincidence[od_subleading_area_id]);

    
  m_hists->BookFillHist(text+"od_leading_z", 20, 0., 10, (*m_event->m_odHGPulses)->pulseZPosition[od_max_area_id]);
  m_hists->BookFillHist(text+"od_leading_theta", 20, 0., 360, (*m_event->m_odHGPulses)->pulseTheta[od_max_area_id]);
  m_hists->BookFillHist(text+"od_leading_phi", 20, 0., 360, (((*m_event->m_odHGPulses)->pulseLadderPosition[od_max_area_id]-0.5)*(360./20.)));

  m_hists->BookFillHist(text+"od_subleading_z", 20, 0., 10, (*m_event->m_odHGPulses)->pulseZPosition[od_subleading_area_id]);
  m_hists->BookFillHist(text+"od_subleading_theta", 20, 0., 360, (*m_event->m_odHGPulses)->pulseTheta[od_subleading_area_id]);
  m_hists->BookFillHist(text+"od_subleading_phi", 20, 0., 360, (((*m_event->m_odHGPulses)->pulseLadderPosition[od_subleading_area_id]-0.5)*(360./20.)));



  
  //float tcp_s1_time=(*m_event->m_tpcHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID];
  float leading_od_time=(*m_event->m_odHGPulses)->pulseStartTime_ns[od_max_area_id];
  float subleading_od_time=(*m_event->m_odHGPulses)->pulseStartTime_ns[od_subleading_area_id];
  double diff_leading=abs(leading_od_time-tcp_s1_time);
  double diff_subleading=abs(subleading_od_time-tcp_s1_time);

  if (tcp_s1_time>leading_od_time)
    diff_leading=tcp_s1_time-leading_od_time;
  else 
    diff_leading=leading_od_time-tcp_s1_time;
 
  if (tcp_s1_time>leading_od_time)
    diff_subleading=tcp_s1_time-subleading_od_time;
  else 
    diff_subleading=subleading_od_time-tcp_s1_time;

  m_hists->BookFillHist(text+"od_pstart_leading_minus_subleading", 30, -1E4, 1E4, diff_leading - diff_subleading);
  
      
  return;
}


// Called before event loop
void ODAnalysis::Initialize() {
  INFO("Initializing ODAnalysis Analysis");
  nEvents=0;
}

// Called once per event
void ODAnalysis::Execute() {
  nEvents++;
  bool verbose=false;
  
  // ----------------------------------------------------
  //calculate OD based quantites by looping over all
  // ----------------------------------------------------
  od_max_coinc=0;
  od_max_coinc_id=-1;
  od_max_area=0;
  od_max_area_id=-1;  
  // float od_max_pulse_area=-1;
  // int od_max_area_id=-1;
  od_subleading_area=0;
  od_subleading_area_id=-1;

  
  for (int i = 0; i < (*m_event->m_odHGPulses)->nPulses; i++) {
    if ((*m_event->m_odHGPulses)->pulseArea_phd[i]<3) continue;  //important for speed!

    if((*m_event->m_odHGPulses)->coincidence[i]>od_max_coinc){
      od_max_coinc=(*m_event->m_odHGPulses)->coincidence[i];
      od_max_coinc_id=i;
    } 
    if((*m_event->m_odHGPulses)->pulseArea_phd[i]>od_max_area){
      od_subleading_area=od_max_area;
      od_subleading_area_id=od_max_area_id;
      od_max_area=(*m_event->m_odHGPulses)->pulseArea_phd[i];
      od_max_area_id=i;
    } 
  }// loop over TPC pulses
  //  std::cout<<"max pulse i"<< max_area_id<<" subleadng "<<subleading_pulse_i<<endl;
  
  od_subleading_leading_ratio=-1;
  if(od_max_area_id>-1 && od_subleading_area_id>-1)
    od_subleading_leading_ratio=(*m_event->m_odHGPulses)->pulseArea_phd[od_subleading_area_id]/(*m_event->m_odHGPulses)->pulseArea_phd[od_max_area_id];

    
  // ----------------------------------------------------
  // Apply Cuts
  // ----------------------------------------------------

  float radius=(float) sqrt(pow((*m_event->m_singleScatter)->x_cm,2)+pow((*m_event->m_singleScatter)->y_cm,2));
  float z_cm= ((*m_event->m_singleScatter)->driftTime_ns)*0.00015; //in cm
  
  FillPlots("cut0_presel_");
  if(!(*m_event->m_singleScatter)->nSingleScatters) return;
  FillPlots("cut1_ss_");
  m_hists->BookFillHist("BLABLABLA", 50, 0., 1, od_subleading_leading_ratio);  
    
  if((*m_event->m_singleScatter)->s1Area_phd>80) return;
  FillPlots("cut2_s1_");

  //if(od_subleading_leading_ratio>0.4) return;
  //  FillPlots("cut3_j1j2_ratio_");

  if((*m_event->m_singleScatter)->s2Area_phd<415) return;
  FillPlots("cut3_s2_");

  if(radius>68.8) return;
  FillPlots("cut4_r_");

  if(z_cm<2 ||z_cm>105 ) return;
  FillPlots("cut5_z_");

    
  //  The fiducial volume cut drawn is the same as for the wimp search: r<68.8cm, 2cm<z<132.6cm.
  //some 
  
  if(verbose){
    std::cout<<" ---------- NEW EVENT ----------"<<std::endl;
    std::cout<<"event-time  "<<(*m_event->m_eventHeader)->triggerTimeStamp_ns/1000<<std::endl;
    
    std::cout<<" is ss: "<< (*m_event->m_singleScatter)->nSingleScatters
	     <<" ss s1id: "<< (*m_event->m_singleScatter)->s1PulseID
	     <<" ss s2 id: "<< (*m_event->m_singleScatter)->s2PulseID
	     <<" ss od prompt n: "<< (*m_event->m_singleScatter)->nODPromptPulses
	     <<" ss od delayed n: "<< (*m_event->m_singleScatter)->nODDelayedPulses
	     <<std::endl;
    
  }    

  // //----------------------------------------------------
  // // Study OD Pulses
  // //----------------------------------------------------
  // maxCoin=0;
  // maxCoinID=-1;  
   
  // if(verbose){
  //   if((*m_event->m_singleScatter)->nODPromptPulses >0 || (*m_event->m_singleScatter)->nODDelayedPulses>0){
  //     std::cout<<"od prompt n: "     << (*m_event->m_singleScatter)->nODPromptPulses
  // 	       <<" ss od delayed n: " << (*m_event->m_singleScatter)->nODDelayedPulses
  // 	       <<std::endl;

  //     for (int i=0; i<(*m_event->m_singleScatter)->nODPromptPulses; i++)
  // 	std::cout<<"prompt pulse id: "<<(*m_event->m_singleScatter)->odPromptPulseIDs[i]
  // 		 <<std::endl;
	
  //     for (int i=0; i<(*m_event->m_singleScatter)->nODDelayedPulses; i++)
  // 	std::cout<<"delayed pulse id: "<<(*m_event->m_singleScatter)->odDelayedPulseIDs[i]
  // 		 <<std::endl;
  //   }
  // }

 
  // int tpc_s1_id=(*m_event->m_singleScatter)->s1PulseID;
  // if((*m_event->m_singleScatter)->nSingleScatters==1 && (*m_event->m_singleScatter)->nODPromptPulses>0 ){
  //   float tcp_s1_time=(*m_event->m_tpcHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID];
  //   m_hists->BookFillHist("tpcS1PulseIDs_time3", 100, -1E6, 1E6, tcp_s1_time);
  //   float od_time=(*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[0]];
  //   float diff=abs(od_time-tcp_s1_time);
  //   m_hists->BookFillHist("diff_tcps1_ODprompt", 50, 0., 100, diff); 
  // }
  // if((*m_event->m_singleScatter)->nSingleScatters==1 && (*m_event->m_singleScatter)->nODDelayedPulses>0){
  //   float tcp_s1_time=(*m_event->m_tpcHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->s1PulseID];
  //   float od_time=(*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[0]];
  //   float diff=od_time-tcp_s1_time;
  //   m_hists->BookFillHist("diff_tcps1_ODdelayed", 50, -400, 400, diff);
  //   m_hists->BookFillHist("diff_tcps1_ODdelayed2", 100, -1E4, 1E4, diff);
  //   m_hists->BookFillHist("diff_tcps1_ODdelayed2b", 100, -2000, 400, diff);
  //   m_hists->BookFillHist("diff_tcps1_ODdelayed3", 100, -1E6, 1E6, diff);
  // }

    
  // //plot info of basic Pulse IDs
  // m_hists->BookFillHist("EventsClassification", 5, 0., 5, 0);
  // if((*m_event->m_singleScatter)->nSingleScatters==1){
  //   m_hists->BookFillHist("EventsClassification", 5, 0., 5, 1);
  //   if((*m_event->m_singleScatter)->nODPromptPulses>0)
  //     m_hists->BookFillHist("EventsClassification", 5, 0., 5, 2);
  //   if((*m_event->m_singleScatter)->nODDelayedPulses>0)
  //     m_hists->BookFillHist("EventsClassification", 5, 0., 5, 3);
      
      
      
  //   m_hists->BookFillHist("s1PulseID", 5, 0., 5, (*m_event->m_singleScatter)->s1PulseID);
  //   m_hists->BookFillHist("nODPromptPulses", 4, 0., 4, (*m_event->m_singleScatter)->nODPromptPulses);
  //   m_hists->BookFillHist("nODDelayedPulses", 10, 0., 10, (*m_event->m_singleScatter)->nODDelayedPulses);
      
  //   for (int i=0; i<(*m_event->m_singleScatter)->nODPromptPulses; i++){
  //     m_hists->BookFillHist("odPromptPulseIDs", 500, 0., 500, (*m_event->m_singleScatter)->odPromptPulseIDs[i]);
  //     m_hists->BookFillHist("odPromptPulseIDs_time", 100, -2000., 2000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[i]]);
  //     m_hists->BookFillHist("odPromptPulseIDs_time2", 100, -100000, 100000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[i]]);
  //     m_hists->BookFillHist("odPromptPulseIDs_time3", 100, -1E6, 1E6,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[i]]);
  //     m_hists->BookFillHist("odPromptPulseIDs_time_vs_area", 1000, -1E6, 1E6, 300, 0, 1000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[i]],
  // 			    (*m_event->m_odHGPulses)->pulseArea_phd[(*m_event->m_singleScatter)->odPromptPulseIDs[i]]);
  //     m_hists->BookFillHist("odPromptPulseIDs_time_vs_totarea", 1000, -1E6, 1E6, 300, 0, 1E6,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odPromptPulseIDs[i]],
  // 			    (*m_event->m_singleScatter)->odPromptArea);

  //   }
      
      
  //   for (int i=0; i<(*m_event->m_singleScatter)->nODDelayedPulses; i++){
  //     m_hists->BookFillHist("odDelayedPulseIDs", 500, 0., 500, (*m_event->m_singleScatter)->odDelayedPulseIDs[i]);
  //     m_hists->BookFillHist("odDelayedPulseIDs_time", 100, -2000, 2000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]]);
  //     m_hists->BookFillHist("odDelayedPulseIDs_time2", 100, -100000, 100000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]]);
  //     m_hists->BookFillHist("odDelayedPulseIDs_time3", 100, -1E6, 1E6,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]]);
  //     m_hists->BookFillHist("odDelayedPulseIDs_time_vs_area", 1000, -1E6, 1E6, 300, 0, 1000,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]],
  // 			    (*m_event->m_odHGPulses)->pulseArea_phd[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]]);
  //     m_hists->BookFillHist("odDelayedPulseIDs_time_vs_totarea", 1000, -1E4, 1E4, 300, 0, 1E4,
  // 			    (*m_event->m_odHGPulses)->pulseStartTime_ns[(*m_event->m_singleScatter)->odDelayedPulseIDs[i]],
  // 			    (*m_event->m_singleScatter)->odPromptArea);            
  //   }
  // }
    
}//execute


// Called after event loop
void ODAnalysis::Finalize() {
  std::cout<<"Processed "<<nEvents<<endl;
}

