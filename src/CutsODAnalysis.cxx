#include "CutsODAnalysis.h"
#include "ConfigSvc.h"

CutsODAnalysis::CutsODAnalysis(EventBase* eventBase) {
  m_event = eventBase;
}

CutsODAnalysis::~CutsODAnalysis() {
  
}

// Function that lists all of the common cuts for this Analysis
bool CutsODAnalysis::ODAnalysisCutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}

